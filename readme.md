<h2>UI test automation using Playwright </h2>

<h4>Feel free to check CI/CD tab for test run results</h4>

<h3>Requirements to implement</h3>
<ul>
<li>Run with <b>mvn clean verify</b> command</li>
<li>Set of tests to cover main Playwright capabilities for UI testing</li>
<li>Set of tests to cover main Playwright capabilities for API testing</li>
<li>Allure reports generation</li>
<li>Parallel test run</li>
<li>Configure GtiLab CI to run tests</li>
<li>Save page screenshot and video on test failure (implemented via JUnit Extension)</li>
</ul>