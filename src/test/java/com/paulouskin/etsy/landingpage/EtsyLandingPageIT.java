package com.paulouskin.etsy.landingpage;

import com.paulouskin.etsy.core.BaseIT;
import com.paulouskin.etsy.pageobjects.LandingPage;
import com.paulouskin.extensions.SaveVideoOnFailureWatcher;
import com.paulouskin.extensions.ScreenshotOnFailureExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(SaveVideoOnFailureWatcher.class)
public class EtsyLandingPageIT extends BaseIT {

    @Test
    public void shouldAcceptDefaultPrivacyPolicySettings() {
        LandingPage landingPage = new LandingPage(page);
        landingPage.goTo();
        landingPage.acceptPrivacyPolicyIfPresent();
        Assertions.assertFalse(landingPage.isPrivacyPolicyVisible());
    }

}
