package com.paulouskin.etsy.core;

import com.microsoft.playwright.*;
import com.paulouskin.playwright.ui.PlaywrightFactory;
import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class BaseIT {
    protected Page page;
    protected PlaywrightFactory factory = new PlaywrightFactory();

    protected Logger logger = LoggerFactory.getLogger(BaseIT.class);
    @BeforeEach
    public void testSetUp(TestInfo info) {
        logger.info("Setting up Playwright UI automation for {}", info.getDisplayName());
        page = factory.initPage("chromium");
    }

    @AfterEach
    public void testCleanUp(TestInfo info) {
        logger.info("Cleaning up Playwright UI automation for {}", info.getDisplayName());
        page.close();
        factory.cleanUp();
    }

}
