package com.paulouskin.etsy.feature.search;

import com.paulouskin.etsy.core.BaseIT;
import com.paulouskin.etsy.pageobjects.LandingPage;
import com.paulouskin.etsy.pageobjects.SearchResultPage;
import com.paulouskin.extensions.SaveVideoOnFailureWatcher;
import com.paulouskin.extensions.ScreenshotOnFailureExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(SaveVideoOnFailureWatcher.class)
public class EtsySearchFeatureIT extends BaseIT {

    @Test
    public void shouldShowResultOfASearchRequest(){
        String searchQuery = "leather bag";
        LandingPage landingPage = new LandingPage(page);
        SearchResultPage searchResultPage = new SearchResultPage(page);
        landingPage.goTo();
        landingPage.acceptPrivacyPolicyIfPresent();
        landingPage.searchFor(searchQuery);
        Assertions.assertTrue(searchResultPage.isSearchResultsVisible());
    }
}
