package com.paulouskin.fakestore.core;

import com.microsoft.playwright.APIRequest;
import com.microsoft.playwright.APIRequestContext;
import com.microsoft.playwright.APIResponse;
import com.microsoft.playwright.Playwright;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseApiIT {

    protected final Logger logger = LoggerFactory.getLogger(BaseApiIT.class);
    protected Playwright playwright;
    protected APIRequest request;
    protected APIResponse response;
    protected APIRequestContext requestContext;
    private static final String HOST = "https://fakestoreapi.com";


    @BeforeEach
    public void setUp(TestInfo info) {
        logger.info("Setting up Playwright for API testing for '{}'.", info.getDisplayName());
        playwright = Playwright.create();
        request = playwright.request();
        logger.info("Setting endpoint to '{}'", HOST);
        requestContext = request.newContext(
                new APIRequest.NewContextOptions()
                        .setBaseURL(HOST)
        );
    }

    @AfterEach
    public void tearDown(TestInfo info) {
        logger.info("Cleaning up Playwright after '{}'", info.getDisplayName());
        requestContext.dispose();
        playwright.close();
    }
}
