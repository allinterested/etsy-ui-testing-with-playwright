package com.paulouskin.fakestore.feature.products;

import com.microsoft.playwright.options.RequestOptions;
import com.paulouskin.fakestore.core.BaseApiIT;
import com.paulouskin.fakestore.dto.CreateProductRequestDTO;
import com.paulouskin.fakestore.dto.CreateProductResponseDTO;
import com.paulouskin.fakestore.dto.GetProductResponseDTO;
import com.paulouskin.playwright.api.ResponseProcessor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class ProductsEndpointApiIT extends BaseApiIT {
    @Test
    public void shouldFetchAllProducts() {
        response = requestContext.get("/products");
        Assertions.assertTrue(response.ok());
    }
    @Test
    public void shouldGetProductUsingId() {
        int productId = 5;
        response = requestContext.get(
                String.format("/products/%s", productId)
        );
        ResponseProcessor.getPrettifiedBodyAsString(response.body()).ifPresent(
                body -> logger.info("Response body: \n {}", body)
        );
        var product = ResponseProcessor.getBodyAsDTO(response.body(), GetProductResponseDTO.class)
                        .orElseThrow();
        Assertions.assertTrue(product.title().contains("Silver Dragon Station"));
    }
    @Test
    public void shouldLimitAmountOfFetchedProducts() {
        int limit = 3;
        response = requestContext.get(
                "/products", RequestOptions.create().setQueryParam("limit", limit)
        );
        Assertions.assertTrue(response.ok());
    }
    @Test
    public void shouldAddNewProductWithPayloadAsMap() {
        Map<String, String> payload = getProductPayload();
        response = requestContext.post("/products", RequestOptions.create().setData(payload));
        ResponseProcessor.getPrettifiedBodyAsString(response.body()).ifPresent(
                body -> logger.info("Response body: \n {}", body)
        );
        Assertions.assertTrue(response.text().contains("id"));
    }

    @Test
    public void shouldAddNewProductWithPayloadAsObject() {
        var productPayload = new CreateProductRequestDTO(
                "Leather belt",
                3.99,
                "Black belt from eco leather",
                "https://linktosomeimage.com",
                "men's clothing"

        );
        response = requestContext.post("/products", RequestOptions.create().setData(productPayload));
        ResponseProcessor.getPrettifiedBodyAsString(response.body()).ifPresent(
                body -> logger.info("Response body: \n {}", body)
        );
        var createdProduct = ResponseProcessor
                .getBodyAsDTO(response.body(), CreateProductResponseDTO.class).orElseThrow();
        Assertions.assertEquals(productPayload.title(), createdProduct.title());
    }
    @Test
    public void shouldUpdateExistingProduct(){
        int productId = 5;
        var payload = getProductPayload();
        payload.computeIfPresent("title", (key, value) -> "Updated " + value);
        response = requestContext.put("/products/" + productId,
                RequestOptions.create().setData(payload));
        logger.info("Response text: {}", response.text());
        Assertions.assertTrue(response.text().contains("Updated"));
    }

    @Test
    public void shouldDeleteProductById() {
        int productId = 3;
        response = requestContext.delete(String.format("/products/%s", productId));
        Assertions.assertTrue(response.text().contains("id"));
    }
    private static Map<String, String> getProductPayload() {
        Map<String, String> payload = new HashMap<>();
        payload.put("title", "Brand new glasses");
        payload.put("price", "25.70");
        payload.put("description", "I wear my sunglasses at night so I can so I can");
        payload.put("image", "https://eu.louisvuitton.com/eng-e1/men/accessories/sunglasses/_/N-t194fv1y");
        payload.put("category", "men's clothing");
        return payload;
    }

}
