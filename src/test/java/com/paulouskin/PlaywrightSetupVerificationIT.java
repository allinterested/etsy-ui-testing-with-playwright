package com.paulouskin;

import com.paulouskin.etsy.core.BaseIT;
import com.paulouskin.extensions.SaveVideoOnFailureWatcher;
import com.paulouskin.extensions.ScreenshotOnFailureExtension;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Story;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

@Epic("Verification test for Playwright configuration")
@ExtendWith(SaveVideoOnFailureWatcher.class)
public class PlaywrightSetupVerificationIT extends BaseIT {

    @Test
    @Story("As a test automation engineer I want Playwright to be configured for further usage")
    @Description("Go to Google.com page and verify title")
    public void shouldGoToGoogleComPage() {
        page.navigate("https://www.google.com");
        assertThat(page).hasTitle("Google");
    }
}
