package com.paulouskin.etsy.pageelements;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;

public class SearchElement {

    public static final String SEARCH_FIELD = "[data-id='search-query']";
    private final Page page;
    private final Locator searchField;


    public SearchElement(Page page) {
        this.page = page;
        searchField = page.locator(SEARCH_FIELD);
    }


    public void searchFor(String query) {
        searchField.clear();
        searchField.fill(query);
        page.keyboard().press("Enter");
    }
}
