package com.paulouskin.etsy.pageobjects;

import com.microsoft.playwright.Page;

public abstract class BasePO {

    protected Page page;

    public BasePO(Page page) {
        this.page = page;
    }
}
