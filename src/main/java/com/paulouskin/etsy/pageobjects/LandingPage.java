package com.paulouskin.etsy.pageobjects;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.TimeoutError;
import com.microsoft.playwright.options.LoadState;
import com.paulouskin.etsy.pageelements.SearchElement;

import java.util.Optional;

public class LandingPage extends BasePO{

    public static final String PRIVACY_POLICY_MODAL = "[data-gdpr-single-choice-accept='true']";
    private final SearchElement searchElement;
    private final String HOST = "https://www.etsy.com";

    public LandingPage(Page page) {
        super(page);
        searchElement = new SearchElement(page);
    }

    public void acceptPrivacyPolicyIfPresent() {
        getPrivacyPolicyAcceptButton().ifPresent(Locator::click);
    }

    public void goTo() {
        page.navigate(HOST);
    }

    public boolean isPrivacyPolicyVisible() {
        page.waitForLoadState(LoadState.NETWORKIDLE);
        return page.isVisible(PRIVACY_POLICY_MODAL);
    }

    public void searchFor(String query) {
        searchElement.searchFor(query);
    }

    private Optional<Locator> getPrivacyPolicyAcceptButton() {
        if (page.isVisible(PRIVACY_POLICY_MODAL)) {
            return Optional.of(page.locator(PRIVACY_POLICY_MODAL));
        } else {
            return Optional.empty();
        }
    }
}
