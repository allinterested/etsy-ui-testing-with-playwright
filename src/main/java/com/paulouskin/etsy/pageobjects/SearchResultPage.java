package com.paulouskin.etsy.pageobjects;

import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.LoadState;

public class SearchResultPage extends BasePO{

    private static final String SEARCH_RESULT_ITEM_LINK_SELECTOR = ".listing-link";
    public SearchResultPage(Page page) {
        super(page);
    }

    public boolean isSearchResultsVisible() {
        page.waitForLoadState(LoadState.NETWORKIDLE);
        return page.locator(SEARCH_RESULT_ITEM_LINK_SELECTOR).nth(0).isVisible();
    }
}
