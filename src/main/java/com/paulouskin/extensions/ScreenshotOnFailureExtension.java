package com.paulouskin.extensions;

import com.microsoft.playwright.Page;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestExecutionExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Arrays;

public class ScreenshotOnFailureExtension implements TestExecutionExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(ScreenshotOnFailureExtension.class);
    public static final String FAILURE_SCREENSHOTS_FOLDER = "failure-screenshots/";

    @Override
    public void handleTestExecutionException(ExtensionContext extensionContext, Throwable throwable) throws Throwable {
        logger.info("Test failed: {}", extensionContext.getRequiredTestMethod().getName());
        String screenShotName = extensionContext.getRequiredTestMethod().getName() + "_" +
                         Instant.now().toEpochMilli() + ".png";
        logger.info("Failure screenshot stored in: {}", FAILURE_SCREENSHOTS_FOLDER + screenShotName);
        Page page = getPageObjectFromTestClass(extensionContext);
        takePageScreenshot(screenShotName, page);
        throw throwable;
    }

    private static void takePageScreenshot(String screenShotName, Page page) {
        page.screenshot(new Page.ScreenshotOptions()
                .setPath(Paths.get(FAILURE_SCREENSHOTS_FOLDER + screenShotName))
                .setFullPage(true)
        );
    }

    private Page getPageObjectFromTestClass(ExtensionContext extensionContext) throws IllegalAccessException {
        var testClass = extensionContext.getTestClass().orElseThrow(NoClassDefFoundError::new);
        Field f = Arrays.stream(testClass.getSuperclass().getDeclaredFields())
            .filter(fld -> fld.getType() == Page.class)
            .findFirst()
            .orElseThrow(() -> new RuntimeException("No Page class field present in a test class"));
        f.setAccessible(true);
        return (Page) f.get(extensionContext.getTestInstance().orElseThrow());
    }
}
