package com.paulouskin.extensions;

import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class SaveVideoOnFailureWatcher implements TestWatcher, BeforeEachCallback, BeforeAllCallback {

    private List<Path> videoPaths = new ArrayList<>();
    protected final Logger logger = LoggerFactory.getLogger(SaveVideoOnFailureWatcher.class);
    @Override
    public void beforeEach(ExtensionContext extensionContext) throws Exception {
        videoPaths = Files.walk(Paths.get("videos"))
                .filter(Files::isRegularFile)
                .toList();
    }

    @Override
    public void testSuccessful(ExtensionContext context) {
        logger.info("Deleting video for passed test '{}'", context.getDisplayName());
        deletePassTestVideo();
    }

    private void deletePassTestVideo() {
        try {
            Files.walk(Paths.get("videos"))
                .filter(Files::isRegularFile)
                .filter(vid -> !videoPaths.contains(vid))
                .forEach(this::deleteVideoFile);
        } catch (IOException e) {
            throw new RuntimeException("Error occurred while deleting video for failing test");
        }
    }

    private void deleteVideoFile(Path pathToVideo){
        try {
            Files.deleteIfExists(pathToVideo);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Override
    public void beforeAll(ExtensionContext extensionContext) {
        logger.info("Creating folder to store videos for failing tests");
        createVideosFolder();
    }

    private void createVideosFolder() {
        String videosFolder = "videos";
        try {
            if (Files.notExists(Paths.get(videosFolder))) {
                Files.createDirectory(Paths.get(videosFolder));
            }
        } catch (IOException e) {
            throw new RuntimeException("Can not create directory for test failure videos");
        }
    }
}
