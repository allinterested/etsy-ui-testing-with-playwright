package com.paulouskin.playwright.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Optional;

public class ResponseProcessor {

    private final static ObjectMapper mapper = new ObjectMapper();

    public static Optional<String> getPrettifiedBodyAsString(byte[] body) {
        try {
            JsonNode bodyAsJson = mapper.readTree(body);
            return Optional.ofNullable(bodyAsJson.toPrettyString());
        } catch (IOException e) {
            return Optional.empty();
        }
    }

    public static  <T> Optional<T> getBodyAsDTO(byte[] body, Class<T> clazz){
        String jsonString = new String(body, Charset.defaultCharset());
        try {
            return Optional.of(mapper.readValue(jsonString, clazz));
        } catch (JsonProcessingException e) {
            return Optional.empty();
        }
    }
}

