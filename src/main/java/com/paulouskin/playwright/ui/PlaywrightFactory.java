package com.paulouskin.playwright.ui;

import com.microsoft.playwright.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Paths;

public class PlaywrightFactory {

    public static final int VIEWPORT_WIDTH = 1600;
    public static final int VIEWPORT_HEIGHT = 1200;
    private Playwright playwright;
    private Browser browser;
    private BrowserContext context;

    private ThreadLocal<Playwright> tlPlaywright = new ThreadLocal<>();
    private ThreadLocal<Browser> tlBrowser = new ThreadLocal<>();
    private ThreadLocal<BrowserContext> tlBrowserContext = new ThreadLocal<>();
    private ThreadLocal<Page> tlPage = new ThreadLocal<>();

    protected static Logger logger = LoggerFactory.getLogger(PlaywrightFactory.class);

    public Page initPage(String browserName){
        logger.info("Initializing Playwright");
        tlPlaywright.set(Playwright.create());
        logger.info("Browser headless mode is " + System.getProperty("headless"));
        boolean isHeadless = Boolean.parseBoolean(System.getProperty("headless"));
        logger.info("Selected browser - " + browserName);
        tlBrowser.set ( switch (browserName.toLowerCase()){
            case "safari" ->
                getPlaywright().webkit().launch(
                        new BrowserType.LaunchOptions().setHeadless(isHeadless));
            default ->
                getPlaywright().chromium().launch(
                        new BrowserType.LaunchOptions().setHeadless(isHeadless));
        });
        tlBrowserContext.set(getBrowser().newContext(
                new Browser.NewContextOptions()
                    .setViewportSize(VIEWPORT_WIDTH, VIEWPORT_HEIGHT)
                        .setRecordVideoDir(Paths.get("videos/"))
                        .setRecordVideoSize(VIEWPORT_WIDTH, VIEWPORT_HEIGHT)
                )
        );
        tlPage.set(getContext().newPage());
        return getPage();

    }

    private Page getPage() {
        return tlPage.get();
    }

    private BrowserContext getContext() {
        return tlBrowserContext.get();
    }

    private Browser getBrowser() {
        return tlBrowser.get();
    }

    private Playwright getPlaywright() {
        return tlPlaywright.get();
    }

    public void cleanUp(){
        logger.info("Cleaning up Playwright");
        getContext().close();
        getPlaywright().close();
    }
}
