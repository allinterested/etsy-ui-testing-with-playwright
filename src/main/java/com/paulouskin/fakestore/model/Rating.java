package com.paulouskin.fakestore.model;

public record Rating(double rate, int count) {
}
