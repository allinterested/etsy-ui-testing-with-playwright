package com.paulouskin.fakestore.dto;

public record CreateProductRequestDTO(String title, double price, String description, String image, String category) {
}
