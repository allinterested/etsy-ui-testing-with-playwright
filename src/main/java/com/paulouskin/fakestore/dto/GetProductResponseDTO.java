package com.paulouskin.fakestore.dto;

import com.paulouskin.fakestore.model.Rating;

public record GetProductResponseDTO(String id, String title, double price, String description, String image, String category, Rating rating) {
}
