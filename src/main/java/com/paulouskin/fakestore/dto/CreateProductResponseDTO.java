package com.paulouskin.fakestore.dto;

public record CreateProductResponseDTO(String id,
                                       String title,
                                       double price,
                                       String description,
                                       String image,
                                       String category) {
}
